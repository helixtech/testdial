<?php
class Vendoradmin_model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function loginCheck($email,$password){
			
			$query = $this->db->query("SELECT u.id, concat(u.fname, ' ',u.lname) as name, u.email, u.password, u.status, 'Vendor Admin' AS role
					FROM users as u
					INNER JOIN vendor_admin as s ON s.user_id = u.id
                    WHERE status='1' AND u.email = ? Limit 1", array($email));

			$admin	 = $query->row(0);
			

			if(!$admin)
				return False;



			//$this->load->helper('phpass');
			//$hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);    	
			if(md5($password) == $admin->password){				
				//$this->db->set('lastLogin', date("Y-m-d H:i:s"));
				//$this->db->where('id', $admin->id);
				//$this->db->update('users');
				unset($admin->password);
				return $admin;
			} else {
				return False;
			}
		}

		public function cookieLogin($id){
			$this->db->select('a.id, a.name, a.email, a.password, a.cityId, app_city.name as userCity, a.isActive, a.lastLogin, a.createdAt, a.updatedAt');
			$this->db->from('app_users as a');
			$this->db->join('app_city', 'app_city.id = a.cityId','inner');
			$this->db->where('a.id',$id);
			$this->db->where('a.isActive','1');
			$this->db->limit(1);
			$admin = $this->db->get()->row();

			if(!$admin)
				return False;

			return $admin;
		}

		public function resetForgotPassword($email){
			$this->db->select('id, fname, lname, email, password, status');
			$this->db->from('users');
			$this->db->where('email',$email);
			$this->db->limit(1);
			$admin = $this->db->get()->row();

			if(!$admin)
				return False;

			//$this->load->helper(array('string','phpass'));
			//$password = random_string('alnum',10);
			//$hasher   = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
			//$hash     = $hasher->HashPassword($password);

			//$hash =  password_hash($password, PASSWORD_BCRYPT, ["cost" => 12]);
			$hash =  MD5($password);
	

			$this->db->set('password',$hash);
			$this->db->where('id', $admin->id);
			$this->db->update('users');
			
			$this->load->library('email');
			$this->email->from('no-reply@dialgoa.in', 'DIALGOA');
			$this->email->to($admin->email); 
			$this->email->subject('Your password has been reset');
			$this->email->message("Hi {$admin->name}! \r\n Your new password is: {$password}, please change the password after you login.\r\n Thank you!");
			$this->email->send();

			return True;
		}

	public function isEmailRegistered($email){
		$rows = $this->db->select('u.id')
					->from('vendor_admin')
					->join('users as u',"u.id=vendor_admin.user_id","inner")
					->where('u.email',$email)
					->limit(1)
					->get()
					->result();
	
		return (count($rows) > 0) ? true : false;
	}

	public function saveVendorAdminAsUser($vendoradmin){
		$result   = $this->db->insert('users',$vendoradmin);
		$userId = $this->db->insert_id();

		return $userId;
	}

	public function updateVendorAdminUser($userId, $vendoradmin){
					$this->db->where('id',$userId);
		$res    = $this->db->update('users',$vendoradmin);
			//print $this->db->last_query();			
					
		return $res;
	}

	public function saveVendorAdmin($vendoradmin){
		$result   = $this->db->insert('vendor_admin',$vendoradmin);
		$vendorId = $this->db->insert_id();

		return $vendorId;
	}

	public function updateVendorAdmin($vendorAdminId, $vendoradmin){
				$this->db->where('id',$vendorAdminId);
		$res    = $this->db->update('vendor_admin',$vendoradmin);
						
					
		return $res;
	}

	public function addNewDesignation($other_design){
		$designation   = array(
					'title'=>$other_design,
					'status'=>1
				 );
		$result   = $this->db->insert('designations',$designation);
		$designationId = $this->db->insert_id();

		return $designationId;
	}

	

	public function deleteVendorAdminServices($vendorAdminId){
				$this->db->where('vendor_admin_id',$vendorAdminId);
		$res    = $this->db->delete('vendoradmin_services');
						
					
		return $res;
	}

	public function addVendorAdminService($vendorAdminService){
		$result   = $this->db->insert('vendoradmin_services',$vendorAdminService);
		$vendorAdminServiceId = $this->db->insert_id();

		return $vendorAdminServiceId;
	}

	public function vendorAdmins(){
		$rows = $this->db->select('v.id, user_id, CONCAT(u.fname," ",u.mname," ", u.lname) as name, u.email, u.dob, u.phone1, u.phone2, u.permanent_address, u. temporary_address, u.status, v.designation, v.govt_id, v.joiningdate, v.deleted_at')
					->from('vendor_admin as v')
					->join('users as u',"u.id=v.user_id","inner")
					->where('v.deleted_at IS NULL')
					->order_by('v.id')
					->get()
					->result();
		return $rows;
	}

	public function updateStatus($action,$vendor_admin){
		$status = 0;
		$status = ($action == 'Confirm') ? 1 : $status;
		$status = ($action == 'Pending') ? 0 : $status;
		//$status = ($action == 'Reject') ? -1 : $status;

		if($action == 'Delete'){
			$res    = $this->db->set('deleted_at', date("Y-m-d H:i:s"))
						->where_in('user_id',$vendor_admin)
						->update('vendor_admin');
		}
		else{

			$res    = $this->db->set('status',$status)
						->where_in('id',$vendor_admin)
						->update('users');
		}

		return $res;
	}

	public function updateVendor($vendor,$vendorId){
		$result   = $this->db->set($vendor)
							->where('id',$vendorId)
							->update('vendor_admin');
		return $result;
	}

	public function getVendorAdmin($id){
		$row = $this->db->select('v.id, user_id, v.vendor_id, CONCAT(u.fname," ",u.mname," ", u.lname) as name, u.email, u.dob, u.phone1, u.phone2, u.permanent_address, u. temporary_address, u.status, v.designation, v.govt_id, v.joiningdate, v.deleted_at')
					->from('vendor_admin as v')
					->join('users as u',"u.id=v.user_id","inner")
					->where('v.id',$id)
					->get()
					->row();
		return $row;
	}

	public function getVendorAdminServices($id){
		$result = $this->db->select('*')
					->from('vendoradmin_services as s')
					->where('s.vendor_admin_id',$id)
					->get()
					->result();
		$outArr = array();
		foreach ($result as $row){
			$outArr[] = $row->service_id;
		}

		return $outArr;
		
	}
}