<?php
class Common_model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}


	public function getOptionsForSelectList($tableName){
		$rows = $this->db->select('id, title')
					->from($tableName)
					->where('status',1)
					->order_by('title','ASC')
					->get()
					->result();
		return $rows;
	}

	public function getServicesList($type){
		$rows = $this->db->select('id, name')
					->from('services')
					->where('status',1)
					->where('type',1)
					->order_by('name','ASC')
					->get()
					->result();
		return $rows;
	}

	public function getAllCities(){
		$this->db->select('id, name');
		$this->db->from('cities');
		$this->db->order_by('name');
		$result = $this->db->get()->result_array();
		$outArr = array();
		foreach ($result as $row){
			$outArr[] = $row;
		}

		return $outArr;
	}

	public function getVehicleClassification(){
		$rows = $this->db->distinct()
					->select('manufacture_company, name ')
					->from('vehicles')
					->where('deleted_at',null)
					->order_by('manufacture_company','ASC')
					->order_by('name','ASC')
					->get()
					->result();
		return $rows;
	}

	
}