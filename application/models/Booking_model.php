<?php
class Booking_model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function getStartPoints(){
		$this->db->select('r.id, r.start_city, cs.name as start_city_name, start_addr, distance, duration, r.status');
		$this->db->from('routes as r');
		$this->db->join('cities as cs', "cs.id = r.start_city", "inner");
		$this->db->where('r.status',1);
		$this->db->where('r.deleted_at IS NULL');
		$this->db->order_by('id');
		$result = $this->db->get()->result();

		return $result;
	}

	public function getEndPoints(){
		$this->db->select('r.id, r.end_city, cs.name as end_city_name, end_addr, distance, duration, r.status');
		$this->db->from('routes as r');
		$this->db->join('cities as cs', "cs.id = r.end_city", "inner");
		$this->db->where('r.status',1);
		$this->db->where('r.deleted_at IS NULL');
		$this->db->order_by('id');
		$result = $this->db->get()->result();

		return $result;
	}

	
}