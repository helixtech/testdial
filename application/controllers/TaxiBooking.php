<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TaxiBooking extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->library('session');
		$this->load->helper(array('url','form'));		
		//$this->load->model('users_model', '', TRUE);
		$this->load->model('common_model', '', TRUE);
		$this->load->model('booking_model', '', TRUE);
		$this->load->model('vendor_model', '', TRUE);
		
		//if(!$this->session->userdata('admin'))
		//	redirect('login');
	}

	private function _processFilters(){
		$filters = array();
		$tempArr = array();

		$tempArr['fromDate']        = $this->input->post('from_date');
		$tempArr['toDate']          = $this->input->post('to_date');
		$tempArr['fromLocation']    = $this->input->post('from_location');
		$tempArr['toLocation']      = $this->input->post('to_location');
		$tempArr['segment']         = $this->input->post('trav_segment');
		$tempArr['vehicleclass']    = $this->input->post('veh_classification');
		$tempArr['vehicleType']     = $this->input->post('veh_type');
		$tempArr['carrier']         = $this->input->post('carrier');
		$tempArr['vehicleVersion']  = $this->input->post('veh_version');
		$tempArr['vendor']          = $this->input->post('vendor');
		$tempArr['vehicleSpec']     = $this->input->post('veh_specification');
		$tempArr['vehicleCapacity'] = $this->input->post('capacity');
		$tempArr['departureTime']   = $this->input->post('dept_time');
		$tempArr['dep2Time']        = $this->input->post('dep2_time');
		$tempArr['arrivalTime']     = $this->input->post('arrival_time');

		foreach ($tempArr as $key => $value) {
			if($value){
				$filters[$key] = $value;
			}
		}

		return $filters;
	}

	public function index(){
		$data = array();

		$data['vehicleTypes'] = $this->common_model->getOptionsForSelectList('vehicle_types');
		$data['vehicleClass'] = $this->common_model->getVehicleClassification();
		$data['vendors']      = $this->vendor_model->Vendors();
		$data['startPoints']  = $this->booking_model->getStartPoints();
		$data['endPoints']    = $this->booking_model->getEndPoints();
		$data['vehicles']     = array();

		$filters = $this->_processFilters();

		if(count($filters) > 0){

		}

		$data['endPoints'] = $filters;
		
		$this->load->view('common/header',$data);
		$this->load->view('taxibooking/search',$data);
		$this->load->view('common/footer',$data);
	}

	public function booking(){
		$data = array();

		
		$this->load->view('common/header',$data);
		$this->load->view('taxibooking/booking',$data);
		$this->load->view('common/footer',$data);
	}

	public function confirm(){
		$data = array();

		
		$this->load->view('common/header',$data);
		$this->load->view('taxibooking/confirm',$data);
		$this->load->view('common/footer',$data);
	}

	
}
