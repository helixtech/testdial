<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VendorAdmin extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		$this->load->library('session');
		$this->load->helper(array('url','form'));		
		//$this->load->model('users_model', '', TRUE);
		$this->load->model('common_model', '', TRUE);
		$this->load->model('vendor_model', '', TRUE);
		$this->load->model('vendoradmin_model', '', TRUE);
		
		//if(!$this->session->userdata('admin'))
		//	redirect('login');
	}

	public function index(){
		$data = array();

		$this->load->view('vendoradmin/login',$data);
	}

	public function logincheck(){
		

		$email      = $this->input->post('email');
		$password   = $this->input->post('password');
		$remember   = $this->input->post('remember');
		$admin      = $this->vendoradmin_model->loginCheck($email,$password);

		if($admin===false){
			$re = array("error"=>true,"message"=>"Invalid email and/or password");
			return $this->output->set_content_type('application/json')->set_output(json_encode($re));
		} else {			
			
			if($remember=='1')
				$this->input->set_cookie('mcba_adminid',$admin->id,525600);
			
			$this->session->set_userdata(array('admin'=>$admin));
			$re = array("error"=>false,"message"=>"","admin"=>$admin);
			return $this->output->set_content_type('application/json')->set_output(json_encode($re));
		}
	}

	public function forgotpassword(){
		$this->load->view('vendoradmin/forgot_password');
	}

	public function resetpass(){
		$email = $this->input->post('forgotemail');
		$resp  = $this->login_model->resetForgotPassword($email);

		if($resp)
			$re = array("error"=>true,"message"=>"");
		else
			$re = array("error"=>false,"message"=>"");		

		return $this->output->set_content_type('application/json')->set_output(json_encode($re));
	}

	public function logout(){
		delete_cookie('mcba_adminid');
		$this->session->sess_destroy();

		$this->session->set_userdata('');

		redirect('VendorAdmin');
	}

	public function manage(){
		$data = array();

		$data['adminvendors'] = $this->vendoradmin_model->vendorAdmins();
		$this->load->view('common/header',$data);
		$this->load->view('vendoradmin/list',$data);
		$this->load->view('common/footer',$data);
	}

	public function addnew($vendorId=''){
		$data = array();
		
		if(isset($vendorId) && $vendorId!=''){
			$data['vendorId'] = $vendorId;
			//get vendor details
			$data['vendorDetails'] = $this->vendor_model->getVendor($vendorId);
		}
		else{
			//get all vendors
			$data['allVendors'] = $this->vendor_model->Vendors();
		}
		
		$data['designations'] = $this->common_model->getOptionsForSelectList('designations');
		$data['services'] = $this->common_model->getServicesList('Taxi');
		
		$this->load->view('common/header',$data);
		$this->load->view('vendoradmin/addnew',$data);
		$this->load->view('common/footer',$data);
	}

	public function savenew(){
		
		$vendorId         = $this->input->post('vendorId');
		
		$staffname         = $this->input->post('staffname');
		$vendor_admin_email = $this->input->post('vendor_admin_email');
		$vendor_admin_dob      = $this->input->post('vendor_admin_dob');
		$vendor_admin_peradd        = $this->input->post('vendor_admin_peradd');
		$vendor_admin_tempadd        = $this->input->post('vendor_admin_tempadd');
		
		if($vendor_admin_tempadd == ''){
			$vendor_admin_tempadd = $vendor_admin_peradd;
		}

		$vendor_admin_pricontact  = $this->input->post('vendor_admin_pricontact');
		$vendor_admin_seccontact      = $this->input->post('vendor_admin_seccontact');


		$vendor_admin_joining       = $this->input->post('vendor_admin_joining');
		$desginationId       = $this->input->post('vendor_admin_desig');
		
		$vendor_admin_voter       = $this->input->post('vendor_admin_voter');
	

		if($this->vendoradmin_model->isEmailRegistered($vendor_admin_email)){
			//email alrerady registered
			return $this->output->set_content_type('application/json')
								->set_output(json_encode(array(
                    				'error' => true,
                    				'message'=>'Email already registered'
                    			)));

		} else {

			$this->load->helper(array('string','phpass'));
			$rawPassword = random_string('alnum',10);

			$vendorUser   = array(
					'password'=>MD5($rawPassword),
					'fname'=>$staffname,
					'email'=>$vendor_admin_email,
					'dob'=>date('Y-m-d', strtotime($vendor_admin_dob)),
					'permanent_address'=>$vendor_admin_peradd,
					'temporary_address'=>$vendor_admin_tempadd,
					'phone1'=>$vendor_admin_pricontact,
					'phone2'=>$vendor_admin_seccontact,
					'status'=>0
				 );
			$userId = $this->vendoradmin_model->saveVendorAdminAsUser($vendorUser);

			//if designation is other add designation to designation table
			if($desginationId =='other'){
				//add designation
				$other_design       = $this->input->post('other_design');
				$desginationId = $this->vendoradmin_model->addNewDesignation($other_design);
			}

			
			$vendor   = array(
					'vendor_id'=>$vendorId,
					'user_id'=>$userId,
					'designation'=>$desginationId,
					'govt_id'=>$vendor_admin_voter,
					'joiningdate'=>date('Y-m-d', strtotime($vendor_admin_joining))
				 );

			$vendorId = $this->vendoradmin_model->saveVendorAdmin($vendor);

			//add vendor services
			$services       = $this->input->post('services');
			
			foreach ($services as $eachService) {

				$vendorAdminService   = array(
					'vendor_admin_id'=>$vendorId,
					'service_id'=>$eachService
				 );
				$this->vendoradmin_model->addVendorAdminService($vendorAdminService);
			}
			
			//send mail to vendor admin with password
			//---------------------------------------------------------------------------------------------------
			$this->load->library('email');
			$this->email->from('no-reply@dialgoa.com', 'DIALGOA');
			$this->email->to($vendor_admin_email); 
			$this->email->subject('Thank you for registering with DIALGOA');
			//$siteUrl = site_url('login');

			$this->email->message("Hi {$staffname}! \r\n\r\nThank you for registering as Vendor Admin with DIALGOA.\r\n\r\nYour password for the account is: {$rawPassword}, please change the password after you login.\r\n\r\nThank you!\r\DIALGOA TEAM");
			$this->email->send();
			//----------------------------------------------------------------------------------------------------

			$this->session->set_flashdata('successMsg', 'Vendor Admin added successfully.');

			return $this->output->set_content_type('application/json')
								->set_output(json_encode(array(
                    				'error' => false,
                    				'message'=>'VendorAdmin registration successful'
                    			)));
		}
	}

	public function edit($vendoradminId){
		$data = array();

		$data['vendoradminId'] = $vendoradminId;
		$data['vendorAdmin']   = $this->vendoradmin_model->getVendorAdmin($vendoradminId);
		$data['vendorAdminServices']   = $this->vendoradmin_model->getVendorAdminServices($vendoradminId);
		
		//print_r($data['vendorAdmin']);

		$data['vendorId'] = $data['vendorAdmin']->vendor_id;
		$data['vendorDetails'] = $this->vendor_model->getVendor($data['vendorAdmin']->vendor_id);
		
		$data['designations'] = $this->common_model->getOptionsForSelectList('designations');
		$data['services'] = $this->common_model->getServicesList('Taxi');

		$this->load->view('common/header',$data);
		$this->load->view('vendoradmin/edit',$data);
		$this->load->view('common/footer',$data);
	}

	/*public function delete(){
		$vendorId = $this->input->post('vendorId');
		$result   = $this->vendoradmin_model->deleteVendorAdmin($vendorId);
		
		if($result){
			return $this->output->set_content_type('application/json')
								->set_output(json_encode(array('error' => false,'message'=>'VendorAdmin deleted.')));
		} else {
			return $this->output->set_content_type('application/json')
								->set_output(json_encode(array('error' => true,'message'=>'Error! Please try again.')));
		}
	}


	public function approval(){
		$data = array();

		$data['vendors'] = $this->vendoradmin_model->VendorAdmins();
		$this->load->view('common/header',$data);
		$this->load->view('vendor/approval',$data);
		$this->load->view('common/footer',$data);
	}*/



	public function update(){
		
		$vendorAdminId         = $this->input->post('vendorAdminId');
		$userId         = $this->input->post('userId');

		$vendorId         = $this->input->post('vendorId');
		$oldEmail         = $this->input->post('oldEmail');
		
		$staffname         = $this->input->post('staffname');
		$vendor_admin_email = $this->input->post('vendor_admin_email');
		$vendor_admin_dob      = $this->input->post('vendor_admin_dob');
		$vendor_admin_peradd        = $this->input->post('vendor_admin_peradd');
		$vendor_admin_tempadd        = $this->input->post('vendor_admin_tempadd');
		
		if($vendor_admin_tempadd == ''){
			$vendor_admin_tempadd = $vendor_admin_peradd;
		}

		$vendor_admin_pricontact  = $this->input->post('vendor_admin_pricontact');
		$vendor_admin_seccontact      = $this->input->post('vendor_admin_seccontact');

		$vendor_admin_joining       = $this->input->post('vendor_admin_joining');
		$desginationId       = $this->input->post('vendor_admin_desig');
		
		$vendor_admin_voter       = $this->input->post('vendor_admin_voter');
	

		if($vendor_admin_email != $oldEmail)
		{
			if($this->vendoradmin_model->isEmailRegistered($vendor_admin_email)){
				//email alrerady registered
				return $this->output->set_content_type('application/json')
									->set_output(json_encode(array(
	                    				'error' => true,
	                    				'message'=>'Email already registered'
	                    			)));
			}	
		}
		
		$vendorUser   = array(
				'fname'=>$staffname,
				'email'=>$vendor_admin_email,
				'dob'=>date('Y-m-d', strtotime($vendor_admin_dob)),
				'permanent_address'=>$vendor_admin_peradd,
				'temporary_address'=>$vendor_admin_tempadd,
				'phone1'=>$vendor_admin_pricontact,
				'phone2'=>$vendor_admin_seccontact
			 );
		$result = $this->vendoradmin_model->updateVendorAdminUser($userId, $vendorUser);

		//if designation is other add designation to designation table
		if($desginationId =='other'){
			//add designation
			$other_design       = $this->input->post('other_design');
			$desginationId = $this->vendoradmin_model->addNewDesignation($other_design);
		}

		
		$vendor   = array(
				'designation'=>$desginationId,
				'govt_id'=>$vendor_admin_voter,
				'joiningdate'=>date('Y-m-d', strtotime($vendor_admin_joining))
			 );

		$res = $this->vendoradmin_model->updateVendorAdmin($vendorAdminId, $vendor);

		//add vendor services
		$services       = $this->input->post('services');
		
		//dlete all existing services assigned 
		$this->vendoradmin_model->deleteVendorAdminServices($vendorAdminId);

		foreach ($services as $eachService) {

			$vendorAdminService   = array(
				'vendor_admin_id'=>$vendorAdminId,
				'service_id'=>$eachService
			 );
			$this->vendoradmin_model->addVendorAdminService($vendorAdminService);
		}
		
		
	
		if($result){
			$this->session->set_flashdata('successMsg', 'VendorAdmin updated');
			return $this->output->set_content_type('application/json')
								->set_output(json_encode(array(
                    				'error' => false,
                    				'message'=>'VendorAdmin updated'
                    			)));
        } else {
        	$this->session->set_flashdata('errorMsg', 'Error! please try again');
        	return $this->output->set_content_type('application/json')
								->set_output(json_encode(array(
                    				'error' => true,
                    				'message'=>'Error, Please try again'
                    			)));
        }
		
	}

	public function statuschange(){
		$vendoradmins = $this->input->post('vendoradmins');
		$action  = trim($this->input->post('action'));

		if(count($vendoradmins) > 0 && in_array($action, array('Confirm','Pending','Delete'))){
			$res = $this->vendoradmin_model->updateStatus($action, $vendoradmins);

			if($res){
				return $this->output->set_content_type('application/json')
								->set_output(json_encode(array(
                    				'error' => false,
                    				'message'=>'vendoradmins updated successfully'
                    			)));
			} else {
				return $this->output->set_content_type('application/json')
								->set_output(json_encode(array(
                    				'error' => true,
                    				'message'=>'vendoradmins not updated'
                    			)));
			}
		} else {
			return $this->output->set_content_type('application/json')
							->set_output(json_encode(array(
                				'error' => true,
                				'message'=>'no vendoradmins or action selected'
                			)));
		}
	}
}
