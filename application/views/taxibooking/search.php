<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCje5c7iWvG9bKvnNri86pPT3-QZNy97ZE&libraries=places"></script>
<link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php print base_url('dist/multirange-gh-pages/multirange.css'); ?>" rel="stylesheet">
<script src="<?php print base_url('dist/multirange-gh-pages/multirange.js'); ?>"></script>

<!-- MetisMenu CSS -->
<link href="<?php print base_url('dist/metisMenu/metisMenu.min.css'); ?>" rel="stylesheet">

<!-- Custom Fonts -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">

<style>
    .overlay {
        height: 0%;
        width: 100%;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: grey;
        overflow-y: hidden;
        transition: 0.5s;
    }
    .overlay-content {
        margin-left:13%;
        position: relative;
        top: 20%;
        width: 75%;
        text-align: center;
        margin-top: 30px;
    }
    .overlay a {
        text-decoration: none;
        font-size: 36px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }
    .special {
        font-size: 15px !important;
    }
    .overlay a:hover, .overlay a:focus {
        color: #f1f1f1;
    }
    .overlay .closebtn {
        position: absolute;
        top: 60px;
        right: 30px;
        font-size: 40px;
        color:black;
    }
    #myNav a:hover {
        text-decoration: none !important;
        color: black !important;
    }
    @media screen and (max-height: 450px) {
        .overlay {overflow-y: auto;}
        .overlay a {font-size: 20px}
        .overlay .closebtn {
            font-size: 40px;
            top: 15px;
            right: 35px;
        }
    }
</style>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="alert alert-dismissible" id="div-taxibooking-list-alert" style="display: none;"></div>
        
        <?php if($this->session->flashdata('successMsg')){ ?>
            <div class="alert alert-success alert-dismissible" id="alert-success"">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <h4><i class="icon fa fa-check"></i>Success!</h4>
                <?php echo $this->session->flashdata('successMsg'); ?>
            </div>
        <?php } if($this->session->flashdata('errorMsg')){ ?>
            <div class="alert alert-danger alert-dismissible" id="alert-error"">
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                <h4><i class="icon fa fa-warning"></i>Error!</h4>
                <span id="message-alert-error">Some error occured, please try again later</span>
            </div>
        <?php } ?>
        
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <ul class="nav nav-pills">
                    <li class="active"><a href="#">Taxi</a></li>                    
                    <li><a href="#">Self-drive</a></li>
                    <li><a href="#">Activities</a></li>
                </ul>
            </div>
            <div class="col-lg-4">
                <!-- dont put anything here -->
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header">Add Booking</h4>
            </div>
        </div>

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <form role="form" method="POST" id="frm-taxibooking" name="frm-taxibooking" action="<?php print site_url('taxiBooking/index'); ?>">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>From Date</label>
                                        <input data-provide="datepicker" class="form-control" type="text" id="from_date" name="from_date" required="required" data-date-start-date="0d">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>To Date</label>
                                        <input data-provide="datepicker" class="form-control" type="text" id="to_date" name="to_date" required="required" data-date-start-date="0d">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group" style="margin-top:25px;">
                                        <input class="btn btn-primary" type="submit" name="find" value="Find"/>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Search</label>
                                        <input class="form-control" type="text" name="search_query" id="search_query">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group" style="margin-top:25px;">
                                        <a class="btn btn-danger" onclick="openNav()">Filter<i class="fa fa-filter fa-fw"></i> </a>
                                    </div>
                                </div>
                                

                                <div id="myNav" class="overlay" style="z-index: 999;">
                                    <a class="closebtn" style="cursor: pointer;" onclick="closeNav()">&times;</a>
                                    <div class="overlay-content">
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <label>PickUp Location</label><br>
                                                <select class="selectpicker" multiple data-live-search="true" data-width="120px" name="from_location" id="from_location">
                                                    <?php foreach($startPoints AS $point): ?>
                                                        <option class="special" value="<?php print $point->id; ?>"><?php print $point->start_city_name; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <label>Drop Location</label><br>
                                                <select class="selectpicker" multiple data-live-search="true" data-width="120px" name="to_location" id="to_location">
                                                    <?php foreach($endPoints AS $point): ?>
                                                        <option class="special" value="<?php print $point->id; ?>"><?php print $point->end_city_name; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <label>Dept Time</label><br>
                                                <select class="selectpicker" multiple data-live-search="true" data-width="120px" name="dept_time" id="dept_time">
                                                    <option class="special">06.00-11.59</option>
                                                    <option class="special">12.00-17.59</option>
                                                    <option class="special">18.00-11.59</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <label>Travel Segment</label><br>
                                                <select class="selectpicker" multiple data-live-search="true" data-width="120px" name="trav_segment" id="trav_segment">
                                                    <option class="special">Fixed Point</option>
                                                    <option class="special">Point to Point</option>
                                                    <option class="special">Full Day</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2 wrd-len">
                                                <label><span >Vehicle Classification</span></label><br>
                                                <select class="selectpicker" multiple data-live-search="true" data-width="120px" name="veh_classification" id="veh_classification">
                                                    <?php foreach($vehicleTypes AS $vehType): ?>
                                                        <option class="special" value="<?php print $vehType->id; ?>"><?php print $vehType->title; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <label>Vehicle Type</label><br>
                                                <select class="selectpicker" multiple data-live-search="true" data-width="120px" name="veh_type" id="veh_type">
                                                    <option class="special">AC</option>
                                                    <option class="special">Non AC</option>
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row top-height">
                                            <div class="col-lg-1">
                                            </div>
                                            <div class="col-lg-2">
                                                <label>Carrier</label><br>
                                                <select class="selectpicker " multiple data-live-search="true" data-width="120px" name="carrier" id="carrier">
                                                    <option class="special">Available</option>
                                                    <option class="special">Not Available</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <label>Version</label><br>
                                                <select class="selectpicker" multiple data-live-search="true" data-width="120px" name="veh_version" id="veh_version">
                                                    <option class="special">Petrol</option>
                                                    <option class="special">Diesel</option>
                                                    <option class="special">Gas</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <label>Vendor Operator</label><br>
                                                <select class="selectpicker" multiple data-live-search="true" data-width="200px" name="vendor" id="vendor">
                                                    <?php foreach($vendors AS $vendor): ?>
                                                        <option class="special" value="<?php print $vendor->id; ?>"><?php print $vendor->name; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-2 wrd-len">
                                                <label>Vehicle Specification</label><br>
                                                <select class="selectpicker" multiple data-live-search="true" data-width="120px" name="veh_specification" id="veh_specification">
                                                    <?php foreach($vehicleClass AS $vehClass): ?>
                                                        <option class="special" value="<?php print $vehClass->manufacture_company; ?>-<?php print $vehClass->name; ?>">
                                                            <?php print $vehClass->manufacture_company; ?> <?php print $vehClass->name; ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <label>Capacity</label><br>
                                                <select class="selectpicker" multiple data-live-search="true" data-width="120px" name="capacity" id="capacity">
                                                    <option class="special">01-02</option>
                                                    <option class="special">03-04</option>
                                                    <option class="special">05-07</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-1">
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-1">
                                                <label><h5 style="margin-top:30px;">0 Hr</h5></label>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="slidecontainer">
                                                    <p>Departure Time:</p>
                                                    <input type="range" multiple value="10,80"  name="dep2_time" id="dep2_time">
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                <label><h5 style="margin-top:30px;">24 Hr</h5></label>
                                            </div>
                                            <div class="col-lg-2">
                                                <!-- <input style="background-color:grey" id="ex2" type="text" class="span2" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="1" data-slider-value="[250,450]"/> -->
                                            </div>
                                            <div class="col-lg-1">
                                                <label><h5 style="margin-top:30px;">0 Hr</h5></label>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="slidecontainer">
                                                    <p>Arrival Time:</p>
                                                    <input type="range" multiple value="5,80" name="arrival_time" id="arrival_time">
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                <label><h5 style="margin-top:30px;">24 Hr</h5></label>
                                            </div>
                                            
                                        </div>
                                        <div class="row top-height ">
                                            <div class="col-xs-4">
                                            </div>
                                            <div class="col-xs-2">
                                                <div class="form-group" style="margin-top:25px;;">
                                                    <a class="btn btn-default special">Apply<i class="fa fa-search-minus fa-fw"></i> </a>
                                                </div>
                                            </div>
                                            <div class="col-xs-2">
                                                <div class="form-group" style="margin-top:25px;">
                                                    <a class="btn btn-default special">Reset<i class="fa fa-times fa-fw"></i> </a>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-xs-4">
                                            </div>
                                            <div class="col-xs-4">
                                                <!-- <span>4 Vehicles Found</span> -->
                                            </div>
                                            <div class="col-xs-4">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <br>

                        <!--

                        <div class="search">
                            <div class="row">
                                <div class="col-lg-5">
                                </div>
                                <div class="col-lg-2">
                                    <label>Your Selection</label>
                                </div>
                                <div class="col-lg-5">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-2">
                                        <label>Start Point</label>
                                        <span>Margao</span>
                                    </div>
                                    <div class="col-lg-2">
                                        <label>End Point</label>
                                        <span>Panaji</span>
                                    </div>
                                    <div class="col-lg-2">
                                        <label>Type of Trip</label>
                                        <span>One Way</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Type of Vehicle</label>
                                        <span>AC Maruti WagonR</span>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Vehicle Classification</label>
                                        <span>Mini</span>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="col-lg-4">
                                        </div>
                                        <div class="col-lg-2">
                                            <label>Seating Capacity</label>
                                            <span>04</span>
                                        </div>
                                        <div class="col-lg-2">
                                            <label>Carrier</label>
                                            <span>Available</span>
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row" style="margin:3px;">
                                    <div class="col-lg-12">
                                        <div class="navbar navbar-default">
                                            <div class="container">
                                                <div class="navbar-collapse collapse" id="navbar-filter">
                                                    <form class="navbar-form" role="search">
                                                        <div class="col-lg-3">
                                                            <label style="margin-top: 20px;margin-left: -30px;">Sort By</label>
                                                        </div>
                                                        <div class="col-lg-3" style="margin-top: 12px;margin-left: -10px;">
                                                            <div class="form-group">
                                                                <select name="filter_type" id="filter_type" class="form-control">
                                                                    <option value="">Select</option>
                                                                    <option value="date">Fare-Low to High</option>
                                                                    <option value="like_count">Fare-High to Low</option>
                                                                    <option value="like_count">Rating-High to Low</option>
                                                                    <option value="like_count">Rating-Low to High</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin:3px;">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#home" data-toggle="tab">11-04-2018</a></li>
                                                    <li><a href="#profile" data-toggle="tab">12-04-2018</a></li>
                                                    <li><a href="#messages" data-toggle="tab">13-04-2018</a></li>
                                                    <li><a href="#settings" data-toggle="tab">14-03-2018</a></li>
                                                </ul>

                                                <div class="tab-content">
                                                    <div class="tab-pane fade in active" id="home">
                                                        <h4>No Vehicles Found</h4>
                                                    </div>
                                                    <div class="tab-pane fade" id="profile">
                                                        <div class="card">
                                                            <a class="button button-primary" style="float:right;cursor: pointer;" onclick="show_commission()">Show/Hide Commission</a>
                                                            <span  data-toggle="collapse" data-target="#demo">
                                                                <h2><b>Innova</b></h2>
                                                                <h3 class="btn btn-danger" style="float:right;background-color:red;font-size:20px;padding-top: 2px;padding-bottom: 2px;">2</h3>
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6">
                                                                        <div class="row">
                                                                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 " >
                                                                                <div class="center" >
                                                                                    <h4><b>Make:</b> Toyota</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12"  data-toggle="collapse" data-target="#demo">
                                                                                <div class="center">
                                                                                    <h4><b>Version:</b>Petrol</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12"  data-toggle="collapse" data-target="#demo">
                                                                                <div class="center">
                                                                                    <h4><b>Seating capacity:</b> 05</h4>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6">
                                                                    </div>
                                                                </div>
                                                            </span>
                                                            
                                                            <div class="collapse" id="demo">
                                                                <hr>
                                                                <div class="card2">
                                                                    <div class="row">
                                                                        <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                                                                            <p><span class="theme-color">VendorName:</span> Laxmi Travels (Dilesh Khandeparkar)</p>
                                                                            <p><span class="theme-color">Vehicle ID:</span> 00152</p>
                                                                            <p><span class="theme-color">Type of Vehicle:</span> Toyota Innova</p>
                                                                            <p><span class="theme-color">Contact No.:</span> 9326888888/9876567654</p>
                                                                            <p><span class="theme-color">Vehicle No.:</span> GA/05/T/0237</p>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                                                                            <div class="car-gallery">
                                                                                <div class="row parent-container">
                                                                                    <div class="col-md-3 col-xs-6 col-sm-4 col-lg-8">
                                                                                        <a href="https://auto.ndtvimg.com/car-images/big/toyota/innova-crysta/toyota-innova-crysta.jpg?v=14" data-lightbox="roadtrip"><img class="img-responsive" src="https://auto.ndtvimg.com/car-images/big/toyota/innova-crysta/toyota-innova-crysta.jpg?v=14" alt="" ></a>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-xs-6 col-sm-4 col-lg-3" style="visibility:hidden">
                                                                                        <a href="https://auto.ndtvimg.com/car-images/big/toyota/innova-crysta/toyota-innova-crysta.jpg?v=14" data-lightbox="roadtrip"><img class="img-responsive" src="https://auto.ndtvimg.com/car-images/big/toyota/innova-crysta/toyota-innova-crysta.jpg?v=14" alt="" ></a>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-xs-6 col-sm-4 col-lg-3" style="visibility:hidden">
                                                                                        <a href="https://auto.ndtvimg.com/car-images/big/toyota/innova-crysta/toyota-innova-crysta.jpg?v=14" data-lightbox="roadtrip"><img class="img-responsive" src="https://auto.ndtvimg.com/car-images/big/toyota/innova-crysta/toyota-innova-crysta.jpg?v=14" alt="" ></a>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-xs-6 col-sm-4 col-lg-3">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                                                                            <div class="info">
                                                                                <div class="row">
                                                                                    <div class="col-md-8 col-sm-6 col-xs-12 col-lg-8">
                                                                                        <p><span class="theme-color">Dept time:</span> 07:00 PM</p>
                                                                                        <p><span class="theme-color">Arrival/End time</span> 08.00 AM</p>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-6 col-xs-12 col-lg-4">
                                                                                        <p><span class="theme-color">Starting fare:</span> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Rs. 500.00</p>
                                                                                    </div>
                                                                                </div>
                                                                                <p><span class="theme-color">Current location:</span> Madgao KTC Bus stand</p>
                                                                                <p><span class="theme-color">Seating capacity</span> 05</p>
                                                                                <p class="commission"><span class="theme-color">Commission.:</span> 5%</p>
                                                                                <p type="submit" class="theme-color-link right " style="cursor: pointer;" data-toggle="collapse" data-target="#view">View All Fare</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="collapse" id="view">
                                                                    <div class="row">
                                                                        <div class="col-lg-12 col-md-12">
                                                                            <table class="table" border="1" height="400">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th ><b>Travel segment</b></th>
                                                                                        <th>Service Type</th>
                                                                                        <th>Trip type</th>
                                                                                        <th>Fare Type</th>
                                                                                        <th>Fare</th>
                                                                                        <th>Above Km</th>
                                                                                        <th> Waiting Charges(Per Hr)</th>
                                                                                        <th>Night charges(Per Hr)</th>
                                                                                        <th><b>Book</b></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td  rowspan="2" height="50"><b>North Goa Tour</b></td>
                                                                                        <td colspan="2" rowspan="2">Local</td>
                                                                                        <td>AC Fare</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td  rowspan="2"><b>South Goa Tour</b></td>
                                                                                        <td colspan="2" rowspan="2">Local</td>
                                                                                        <td>AC Fare</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="4"><b>Fixed Point</b></td>
                                                                                        <td rowspan="2">Local</td>
                                                                                        <td>One way</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Round</td>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 400/-</td>
                                                                                        <td>Rs 600/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2">Outstation</td>
                                                                                        <td>One Way</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Round trip</td>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 400/-</td>
                                                                                        <td>Rs 600/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>   
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="4"><b>Point to Point</b></td>
                                                                                        <td rowspan="2">Local</td>
                                                                                        <td>One way</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Round</td>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 400/-</td>
                                                                                        <td>Rs 600/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2">Outstation</td>
                                                                                        <td>One Way</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Round trip</td>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 400/-</td>
                                                                                        <td>Rs 600/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="4"><b>Full day</b>
                                                                                            <p class="mini">(All four wheeler minimum of 80km and 100km for all six wheeler )</p>
                                                                                        </td>
                                                                                        <td rowspan="2" colspan="2">Local</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 150/-</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 13/-</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2" colspan="2">Outstation</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 150/-</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 13/-</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="4"><b>Disposal Vehicle</b>
                                                                                            <p class="mini">(All four wheeler minimum of 80km and 100km for all six wheeler )</p>
                                                                                        </td>
                                                                                        <td rowspan="2" colspan="2">Local</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 150/-</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 13/-</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2" colspan="2">Outstation</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 150/-</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 13/-</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2"><b>Half day</b>
                                                                                            <p class="mini">(All four wheeler minimum of 40km and 60km for all six wheeler )</p>
                                                                                        </td>
                                                                                        <td rowspan="2" colspan="2">Local</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 300/-</td>
                                                                                        <td>Rs 15/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 150/-</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 13/-</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                            
                                                                <hr>
                                                                <div class="card2">
                                                                    <div class="row">
                                                                        <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                                                                            <p><span class="theme-color">VendorName:</span> Kamaxi Travels (Rajesh Khandeparkar)</p>
                                                                            <p><span class="theme-color">Vehicle ID:</span> 00153</p>
                                                                            <p><span class="theme-color">Type of Vehicle:</span> Toyota Innova</p>
                                                                            <p><span class="theme-color">Contact No.:</span> 9326888888/9876567654</p>
                                                                            <p><span class="theme-color">Vehicle No.:</span> GA/05/K/0230</p>
                                                                        </div>

                                                                        <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                                                                            <div class="car-gallery">
                                                                                <div class="row">
                                                                                    <div class="col-md-3 col-xs-6 col-sm-4 col-lg-8">
                                                                                        <a href="https://static2.toyotabharat.com/images/showroom/innova/color/c_h_4.jpg" data-lightbox="roadtrip1">
                                                                                            <img class="img-responsive" src="https://static2.toyotabharat.com/images/showroom/innova/color/c_h_4.jpg" alt="">
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-xs-6 col-sm-4 col-lg-3" style="visibility:hidden">
                                                                                        <a href="https://static2.toyotabharat.com/images/showroom/innova/color/c_h_4.jpg" data-lightbox="roadtrip1">
                                                                                            <img class="img-responsive" src="https://static2.toyotabharat.com/images/showroom/innova/color/c_h_4.jpg" alt="">
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-xs-6 col-sm-4 col-lg-3" style="visibility:hidden">
                                                                                        <a href="https://static2.toyotabharat.com/images/showroom/innova/color/c_h_4.jpg" data-lightbox="roadtrip1">
                                                                                            <img class="img-responsive" src="https://static2.toyotabharat.com/images/showroom/innova/color/c_h_4.jpg" alt="">
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-xs-6 col-sm-4 col-lg-3" style="visibility:hidden">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                    
                                                                        <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                                                                            <div class="info">
                                                                                <div class="row">                                                                
                                                                                    <div class="col-md-8 col-sm-6 col-xs-12 col-lg-8">
                                                                                        <p><span class="theme-color">Dept time:</span> 07:00 PM</p>
                                                                                        <p><span class="theme-color">Arrival/End time</span> 08.00 AM</p>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-6 col-xs-12 col-lg-4">
                                                                                        <p><span class="theme-color">Starting fare:</span> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Rs. 500.00</p>
                                                                                    </div>
                                                                                </div>
                                                            
                                                                                <p><span class="theme-color">Current location:</span> Madgao KTC Bus stand</p>
                                                                                <p><span class="theme-color">Seating capacity</span> 05</p>
                                                                                <p class="commission"><span class="theme-color">Commission.:</span> 5%</p>
                                                                                <p class="theme-color-link right " style="cursor: pointer;" data-toggle="collapse" data-target="#view2">View All Fare</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                            
                                                                <div class="collapse" id="view2">
                                                                    <div class="row">
                                                                        <div class="col-lg-12 col-md-12">
                                                                            <table class="table" border="1" height="400">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th ><b>Travel segment</b></th>
                                                                                        <th>Service Type</th>
                                                                                        <th>Trip type</th>
                                                                                        <th>Fare Type</th>
                                                                                        <th>Fare</th>
                                                                                        <th>Above Km</th>
                                                                                        <th> Waiting Charges(Per Hr)</th>
                                                                                        <th>Night charges(Per Hr)</th>
                                                                                        <th><b>Book</b></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td  rowspan="2" height="50"><b>North Goa Tour</b></td>
                                                                                        <td colspan="2" rowspan="2">Local</td>
                                                                                        <td>AC Fare</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td  rowspan="2"><b>South Goa Tour</b></td>
                                                                                        <td colspan="2" rowspan="2">Local</td>
                                                                                        <td>AC Fare</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="4"><b>Fixed Point</b></td>
                                                                                        <td rowspan="2">Local</td>
                                                                                        <td>One way</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Round</td>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 400/-</td>
                                                                                        <td>Rs 600/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2">Outstation</td>
                                                                                        <td>One Way</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Round trip</td>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 400/-</td>
                                                                                        <td>Rs 600/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="4"><b>Point to Point</b></td>
                                                                                        <td rowspan="2">Local</td>
                                                                                        <td>One way</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Round</td>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 400/-</td>
                                                                                        <td>Rs 600/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2">Outstation</td>
                                                                                        <td>One Way</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Round trip</td>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 400/-</td>
                                                                                        <td>Rs 600/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="4"><b>Full day</b>
                                                                                            <p class="mini">(All four wheeler minimum of 80km and 100km for all six wheeler )</p>
                                                                                        </td>
                                                                                        <td rowspan="2" colspan="2">Local</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 150/-</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 13/-</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2" colspan="2">Outstation</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 150/-</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 13/-</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="4"><b>Disposal Vehicle</b>
                                                                                            <p class="mini">(All four wheeler minimum of 80km and 100km for all six wheeler )</p>
                                                                                        </td>
                                                                                        <td rowspan="2" colspan="2">Local</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 150/-</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 13/-</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                        <td rowspan="2" colspan="2">Outstation</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 150/-</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 13/-</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2"><b>Half day</b>
                                                                                            <p class="mini">(All four wheeler minimum of 40km and 60km for all six wheeler )</p>
                                                                                        </td>
                                                                                        <td rowspan="2" colspan="2">Local</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 300/-</td>
                                                                                        <td>Rs 15/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 150/-</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 13/-</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="messages">
                                                        <div class="card">
                                                            <a class="button button-primary" style="float:right;cursor: pointer;" onclick="show_commission()">Show/Hide Commission</a>
                                                            <span  data-toggle="collapse" data-target="#demo23">
                                                                <h2><b>WagonR</b></h2><h3 class="btn btn-danger" style="float:right;background-color:red;font-size:20px;padding-top: 2px;padding-bottom: 2px;">2</h3>
                                                                <div class="row">
                                                                    <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6">
                                                                        <div class="row">
                                                                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 " >
                                                                                <div class="center" >
                                                                                    <h4><b>Make:</b> Maruti Suzuki</h4>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12"  data-toggle="collapse" data-target="#demo23">
                                                                                <div class="center">
                                                                                    <h4><b>Version:</b>Petrol</h4>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12"  data-toggle="collapse" data-target="#demo23">
                                                                                <div class="center">
                                                                                    <h4><b>Seating capacity:</b> 04</h4>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6"></div>
                                                                </div>
                                                            </span>
                                                            
                                                            
                                                            <div class="collapse" id="demo23">
                                                                <hr>
                                                                <div class="card2">
                                                                    <div class="row">
                                                                        <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                                                                            <p><span class="theme-color">VendorName:</span> Laxmi Travels (Dilesh Khandeparkar)</p>
                                                                            <p><span class="theme-color">Vehicle ID:</span> 00152</p>
                                                                            <p><span class="theme-color">Type of Vehicle:</span> Suzuki WagonR</p>
                                                                            <p><span class="theme-color">Contact No.:</span> 9326888888/9876567654</p>
                                                                            <p><span class="theme-color">Vehicle No.:</span> GA/05/T/0237</p>
                                                                            
                                                                            
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                                                                            <div class="car-gallery">
                                                                                <div class="row">
                                                                                    <div class="col-md-3 col-xs-6 col-sm-4 col-lg-8">
                                                                                        <a href="https://www.marutisuzuki.com//marutiprodcdn.azureedge.net/-/media/images/maruti/marutisuzuki/car/car-profile-shots/midnight-blue-new-repeat-img-(4).ashx?modified=20180122114236" data-lightbox="roadtrip2"><img class="img-responsive" src="https://www.marutisuzuki.com//marutiprodcdn.azureedge.net/-/media/images/maruti/marutisuzuki/car/car-profile-shots/midnight-blue-new-repeat-img-(4).ashx?modified=20180122114236" alt=""></a>
                                                                                        
                                                                                    </div>
                                                                                    <div class="col-md-3 col-xs-6 col-sm-4 col-lg-3" style="visibility:hidden">
                                                                                        <a href="https://www.marutisuzuki.com//marutiprodcdn.azureedge.net/-/media/images/maruti/marutisuzuki/car/car-profile-shots/midnight-blue-new-repeat-img-(4).ashx?modified=20180122114236" data-lightbox="roadtrip2"><img class="img-responsive" src="https://www.marutisuzuki.com//marutiprodcdn.azureedge.net/-/media/images/maruti/marutisuzuki/car/car-profile-shots/midnight-blue-new-repeat-img-(4).ashx?modified=20180122114236" alt=""></a>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-xs-6 col-sm-4 col-lg-3" style="visibility:hidden">
                                                                                        <a href="https://www.marutisuzuki.com//marutiprodcdn.azureedge.net/-/media/images/maruti/marutisuzuki/car/car-profile-shots/midnight-blue-new-repeat-img-(4).ashx?modified=20180122114236" data-lightbox="roadtrip2"><img class="img-responsive" src="https://www.marutisuzuki.com//marutiprodcdn.azureedge.net/-/media/images/maruti/marutisuzuki/car/car-profile-shots/midnight-blue-new-repeat-img-(4).ashx?modified=20180122114236" alt=""></a>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-xs-6 col-sm-4 col-lg-3">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                                                                            <div class="info">
                                                                                <div class="row">
                                                                                    
                                                                                    <div class="col-md-8 col-sm-6 col-xs-12 col-lg-8">
                                                                                        <p><span class="theme-color">Dept time:</span> 07:00 PM</p>
                                                                                        <p><span class="theme-color">Arrival/End time</span> 08.00 AM</p>
                                                                                    </div>
                                                                                    <div class="col-md-4 col-sm-6 col-xs-12 col-lg-4">
                                                                                        <p><span class="theme-color">Starting fare:</span> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Rs. 500.00</p>
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                
                                                                                <p><span class="theme-color">Current location:</span> Madgao KTC Bus stand</p>
                                                                                <p><span class="theme-color">Seating capacity</span> 05</p>
                                                                                <p class="commission"><span class="theme-color">Commission.:</span> 5%</p>
                                                                                <p class="theme-color-link right " style="cursor: pointer;" data-toggle="collapse" data-target="#view11">View All Fare</p>
                                                                            </div>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="collapse" id="view11">
                                                                    <div class="row">
                                                                        <div class="col-lg-12 col-md-12">
                                                                            <table class="table" border="1" height="400">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th ><b>Travel segment</b></th>
                                                                                        <th>Service Type</th>
                                                                                        <th>Trip type</th>
                                                                                        <th>Fare Type</th>
                                                                                        <th>Fare</th>
                                                                                        <th>Above Km</th>
                                                                                        <th> Waiting Charges(Per Hr)</th>
                                                                                        <th>Night charges(Per Hr)</th>
                                                                                        <th><b>Book</b></th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td  rowspan="2" height="50"><b>North Goa Tour</b></td>
                                                                                        <td colspan="2" rowspan="2">Local</td>
                                                                                        <td>AC Fare</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td  rowspan="2"><b>South Goa Tour</b></td>
                                                                                        <td colspan="2" rowspan="2">Local</td>
                                                                                        <td>AC Fare</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="4"><b>Fixed Point</b></td>
                                                                                        <td rowspan="2">Local</td>
                                                                                        <td>One way</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Round</td>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 400/-</td>
                                                                                        <td>Rs 600/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2">Outstation</td>
                                                                                        <td>One Way</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Round trip</td>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 400/-</td>
                                                                                        <td>Rs 600/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="4"><b>Point to Point</b></td>
                                                                                        <td rowspan="2">Local</td>
                                                                                        <td>One way</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Round</td>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 400/-</td>
                                                                                        <td>Rs 600/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2">Outstation</td>
                                                                                        <td>One Way</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Round trip</td>
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 400/-</td>
                                                                                        <td>Rs 600/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="4"><b>Full day</b>
                                                                                            <p class="mini">(All four wheeler minimum of 80km and 100km for all six wheeler )</p>
                                                                                        </td>
                                                                                        <td rowspan="2" colspan="2">Local</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 150/-</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 13/-</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="2" colspan="2">Outstation</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 150/-</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 13/-</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td rowspan="4"><b>Disposal Vehicle</b>
                                                                                            <p class="mini">(All four wheeler minimum of 80km and 100km for all six wheeler )</p>
                                                                                        </td>
                                                                                        <td rowspan="2" colspan="2">Local</td>
                                                                                        <td>AC fare</td>
                                                                                        <td>Rs 500/-</td>
                                                                                        <td>Rs 700/-</td>
                                                                                        <td>=N/A=</td>
                                                                                        <td>Rs 75/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        
                                                                                        <td>Non AC</td>
                                                                                        <td>Rs 150/-</td>
                                                                                        <td>Rs 200/-</td>
                                                                                        <td>Rs 13/-</td>
                                                                                        <td>Rs 50/-</td>
                                                                                        <td>
                                                                                            <div class="row">
                                                                                                <div class="col-lg-6 col-md-6 ">
                                                                                                    <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                    <td rowspan="2" colspan="2">Outstation</td>
                                                                                    <td>AC fare</td>
                                                                                    <td>Rs 500/-</td>
                                                                                    <td>Rs 700/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    
                                                                                    <td>Non AC</td>
                                                                                    <td>Rs 150/-</td>
                                                                                    <td>Rs 200/-</td>
                                                                                    <td>Rs 13/-</td>
                                                                                    <td>Rs 50/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    
                                                                                </tr>
                                                                                <tr>
                                                                                    <td rowspan="2"><b>Half day</b>
                                                                                        <p class="mini">(All four wheeler minimum of 40km and 60km for all six wheeler )</p>
                                                                                    </td>
                                                                                    <td rowspan="2" colspan="2">Local</td>
                                                                                    <td>AC fare</td>
                                                                                    <td>Rs 200/-</td>
                                                                                    <td>Rs 300/-</td>
                                                                                    <td>Rs 15/-</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    
                                                                                    <td>Non AC</td>
                                                                                    <td>Rs 150/-</td>
                                                                                    <td>Rs 200/-</td>
                                                                                    <td>Rs 13/-</td>
                                                                                    <td>Rs 50/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                            <hr>
                                                            <div class="card2">
                                                                <div class="row">
                                                                    <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                                                                        <p><span class="theme-color">VendorName:</span> Kamaxi Travels (Rajesh Khandeparkar)</p>
                                                                        <p><span class="theme-color">Vehicle ID:</span> 00153</p>
                                                                        <p><span class="theme-color">Type of Vehicle:</span> Suzuki WagonR</p>
                                                                        <p><span class="theme-color">Contact No.:</span> 9326888888/9876567654</p>
                                                                        <p><span class="theme-color">Vehicle No.:</span> GA/05/K/0230</p>
                                                                    </div>
                                                                    <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                                                                        <div class="car-gallery">
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-xs-6 col-sm-4 col-lg-8">
                                                                                    <a href="https://media.zigcdn.com/media/photogallery/2013/Aug/maruti-suzuki-stingrya-front-photo-6_640x480.jpg" data-lightbox="roadtrip3"><img class="img-responsive" src="https://media.zigcdn.com/media/photogallery/2013/Aug/maruti-suzuki-stingrya-front-photo-6_640x480.jpg" alt=""></a>
                                                                                </div>
                                                                                <div class="col-md-3 col-xs-6 col-sm-4 col-lg-3" style="visibility:hidden">
                                                                                    <a href="https://media.zigcdn.com/media/photogallery/2013/Aug/maruti-suzuki-stingrya-front-photo-6_640x480.jpg" data-lightbox="roadtrip3"><img class="img-responsive" src="https://media.zigcdn.com/media/photogallery/2013/Aug/maruti-suzuki-stingrya-front-photo-6_640x480.jpg" alt=""></a>
                                                                                </div>
                                                                                <div class="col-md-3 col-xs-6 col-sm-4 col-lg-3" style="visibility:hidden">
                                                                                    <a href="https://media.zigcdn.com/media/photogallery/2013/Aug/maruti-suzuki-stingrya-front-photo-6_640x480.jpg" data-lightbox="roadtrip3"><img class="img-responsive" src="https://media.zigcdn.com/media/photogallery/2013/Aug/maruti-suzuki-stingrya-front-photo-6_640x480.jpg" alt=""></a>
                                                                                </div>
                                                                                <div class="col-md-3 col-xs-6 col-sm-4 col-lg-3">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                                                                        <div class="info">
                                                                            <div class="row">
                                                                                
                                                                                <div class="col-md-8 col-sm-6 col-xs-12 col-lg-8">
                                                                                    <p><span class="theme-color">Dept time:</span> 07:00 PM</p>
                                                                                    <p><span class="theme-color">Arrival/End time</span> 08.00 AM</p>
                                                                                </div>
                                                                                <div class="col-md-4 col-sm-6 col-xs-12 col-lg-4">
                                                                                    <p><span class="theme-color">Starting fare:</span> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Rs. 500.00</p>
                                                                                    
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            
                                                                            <p><span class="theme-color">Current location:</span> Madgao KTC Bus stand</p>
                                                                            <p><span class="theme-color">Seating capacity</span> 05</p>
                                                                            <p class="commission"><span class="theme-color">Commission.:</span> 5%</p>
                                                                            <p class="theme-color-link right " style="cursor: pointer;" data-toggle="collapse" data-target="#view12">View All Fare</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="collapse" id="view12">
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12">
                                                                        <table class="table" border="1" height="400">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th ><b>Travel segment</b></th>
                                                                                    <th>Service Type</th>
                                                                                    <th>Trip type</th>
                                                                                    <th>Fare Type</th>
                                                                                    <th>Fare</th>
                                                                                    <th>Above Km</th>
                                                                                    <th> Waiting Charges(Per Hr)</th>
                                                                                    <th>Night charges(Per Hr)</th>
                                                                                    <th><b>Book</b></th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td  rowspan="2" height="50"><b>North Goa Tour</b></td>
                                                                                    <td colspan="2" rowspan="2">Local</td>
                                                                                    <td>AC Fare</td>
                                                                                    <td>Rs 700/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Non AC</td>
                                                                                    <td>Rs 700/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td  rowspan="2"><b>South Goa Tour</b></td>
                                                                                    <td colspan="2" rowspan="2">Local</td>
                                                                                    <td>AC Fare</td>
                                                                                    <td>Rs 700/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Non AC</td>
                                                                                    <td>Rs 700/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td rowspan="4"><b>Fixed Point</b></td>
                                                                                    <td rowspan="2">Local</td>
                                                                                    <td>One way</td>
                                                                                    <td>AC fare</td>
                                                                                    <td>Rs 500/-</td>
                                                                                    <td>Rs 700/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Round</td>
                                                                                    <td>Non AC</td>
                                                                                    <td>Rs 400/-</td>
                                                                                    <td>Rs 600/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 50/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    
                                                                                </tr>
                                                                                <tr>
                                                                                    <td rowspan="2">Outstation</td>
                                                                                    <td>One Way</td>
                                                                                    <td>AC fare</td>
                                                                                    <td>Rs 500/-</td>
                                                                                    <td>Rs 700/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Round trip</td>
                                                                                    <td>Non AC</td>
                                                                                    <td>Rs 400/-</td>
                                                                                    <td>Rs 600/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 50/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    
                                                                                </tr>
                                                                                <tr>
                                                                                    <td rowspan="4"><b>Point to Point</b></td>
                                                                                    <td rowspan="2">Local</td>
                                                                                    <td>One way</td>
                                                                                    <td>AC fare</td>
                                                                                    <td>Rs 500/-</td>
                                                                                    <td>Rs 700/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Round</td>
                                                                                    <td>Non AC</td>
                                                                                    <td>Rs 400/-</td>
                                                                                    <td>Rs 600/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 50/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    
                                                                                </tr>
                                                                                <tr>
                                                                                    <td rowspan="2">Outstation</td>
                                                                                    <td>One Way</td>
                                                                                    <td>AC fare</td>
                                                                                    <td>Rs 500/-</td>
                                                                                    <td>Rs 700/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Round trip</td>
                                                                                    <td>Non AC</td>
                                                                                    <td>Rs 400/-</td>
                                                                                    <td>Rs 600/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 50/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    
                                                                                </tr>
                                                                                <tr>
                                                                                    <td rowspan="4"><b>Full day</b>
                                                                                        <p class="mini">(All four wheeler minimum of 80km and 100km for all six wheeler )</p>
                                                                                    </td>
                                                                                    <td rowspan="2" colspan="2">Local</td>
                                                                                    <td>AC fare</td>
                                                                                    <td>Rs 500/-</td>
                                                                                    <td>Rs 700/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    
                                                                                    <td>Non AC</td>
                                                                                    <td>Rs 150/-</td>
                                                                                    <td>Rs 200/-</td>
                                                                                    <td>Rs 13/-</td>
                                                                                    <td>Rs 50/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    
                                                                                </tr>
                                                                                <tr>
                                                                                    <td rowspan="2" colspan="2">Outstation</td>
                                                                                    <td>AC fare</td>
                                                                                    <td>Rs 500/-</td>
                                                                                    <td>Rs 700/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    
                                                                                    <td>Non AC</td>
                                                                                    <td>Rs 150/-</td>
                                                                                    <td>Rs 200/-</td>
                                                                                    <td>Rs 13/-</td>
                                                                                    <td>Rs 50/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td rowspan="4"><b>Disposal Vehicle</b>
                                                                                        <p class="mini">(All four wheeler minimum of 80km and 100km for all six wheeler )</p>
                                                                                    </td>
                                                                                    <td rowspan="2" colspan="2">Local</td>
                                                                                    <td>AC fare</td>
                                                                                    <td>Rs 500/-</td>
                                                                                    <td>Rs 700/-</td>
                                                                                    <td>=N/A=</td>
                                                                                    <td>Rs 75/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    
                                                                                    <td>Non AC</td>
                                                                                    <td>Rs 150/-</td>
                                                                                    <td>Rs 200/-</td>
                                                                                    <td>Rs 13/-</td>
                                                                                    <td>Rs 50/-</td>
                                                                                    <td>
                                                                                        <div class="row">
                                                                                            <div class="col-lg-6 col-md-6 ">
                                                                                                <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    
                                                                                </tr>
                                                                                <td rowspan="2" colspan="2">Outstation</td>
                                                                                <td>AC fare</td>
                                                                                <td>Rs 500/-</td>
                                                                                <td>Rs 700/-</td>
                                                                                <td>=N/A=</td>
                                                                                <td>Rs 75/-</td>
                                                                                <td>
                                                                                    <div class="row">
                                                                                        <div class="col-lg-6 col-md-6 ">
                                                                                            <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                
                                                                                <td>Non AC</td>
                                                                                <td>Rs 150/-</td>
                                                                                <td>Rs 200/-</td>
                                                                                <td>Rs 13/-</td>
                                                                                <td>Rs 50/-</td>
                                                                                <td>
                                                                                    <div class="row">
                                                                                        <div class="col-lg-6 col-md-6 ">
                                                                                            <a type="submit" class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td rowspan="2"><b>Half day</b>
                                                                                    <p class="mini">(All four wheeler minimum of 40km and 60km for all six wheeler )</p>
                                                                                </td>
                                                                                <td rowspan="2" colspan="2">Local</td>
                                                                                <td>AC fare</td>
                                                                                <td>Rs 200/-</td>
                                                                                <td>Rs 300/-</td>
                                                                                <td>Rs 15/-</td>
                                                                                <td>Rs 75/-</td>
                                                                                <td>
                                                                                    <div class="row">
                                                                                        <div class="col-lg-6 col-md-6 ">
                                                                                            <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                
                                                                                <td>Non AC</td>
                                                                                <td>Rs 150/-</td>
                                                                                <td>Rs 200/-</td>
                                                                                <td>Rs 13/-</td>
                                                                                <td>Rs 50/-</td>
                                                                                <td>
                                                                                    <div class="row">
                                                                                        <div class="col-lg-6 col-md-6 ">
                                                                                            <a type="submit"  class="btn-primary btn" href="<?php print site_url('TaxiBooking/booking'); ?>">Proceed</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="settings">
                                                <h4>No Vehicles Found</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
        


<script type="text/javascript">
    function showsearch() {
        $(".search").show();
    }
    function show_checkout() {
        $(".checkout").show();
    }
    function openNav() {
        document.getElementById("myNav").style.height = "90%";
    }
    function closeNav() {
        document.getElementById("myNav").style.height = "0%";
    }
    function show_commission() {
        if($('.commission').is(':visible')) {
            $('.commission').hide()
        } else {
            $('.commission').show()
        }
    }
</script>