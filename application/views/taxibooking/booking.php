<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCje5c7iWvG9bKvnNri86pPT3-QZNy97ZE&libraries=places"></script>    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
   <link href="<?php print base_url('dist/multirange-gh-pages/multirange.css'); ?>" rel="stylesheet">
    <script src="<?php print base_url('dist/multirange-gh-pages/multirange.js'); ?>"></script>
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script> -->
    
    <!-- MetisMenu CSS -->
     <link href="<?php print base_url('dist/metisMenu/metisMenu.min.css'); ?>" rel="stylesheet">
     <link href="<?php print base_url('css/process.css'); ?>" rel="stylesheet">
  
  
    <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.2/bootstrap-slider.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.2/css/bootstrap-slider.min.css">-->
    <!-- Custom CSS -->
  
    <!-- Custom Fonts -->
  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
 
  <script type="text/javascript">

    $(document).ready(function() {
        $(".taxi").hide();
        $(".self-drive").hide();
        $(".vehicle-summary").hide();
        var input = document.getElementById('pickupInput');
        var autocomplete = new google.maps.places.Autocomplete(input);
        var input = document.getElementById('dropInput');
        var autocomplete = new google.maps.places.Autocomplete(input);
    });
    
        function show_type(item)
        {
            var value = item.value;
            if(value == 'Self-Drive'){
                $(".self-drive").show();
                $(".taxi").hide();
            }
            else if(value == 'Taxi') {
                $(".taxi").show();
                $(".self-drive").hide();
            }
            else{
                $(".taxi").hide();
                $(".self-drive").hide();
            }
        }
        function vehicle_summary()
        {           
            $(".vehicle-summary").show();    
        }
    
    </script> 


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">    
    <!-- Main content -->
    <section class="content">

    <div class="alert alert-dismissible" id="div-taxibooking-list-alert" style="display: none;"></div>
     <?php
         if($this->session->flashdata('successMsg')){
            ?>
         <div class="alert alert-success alert-dismissible" id="alert-success"">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <h4><i class="icon fa fa-check"></i>Success!</h4> <?php echo $this->session->flashdata('successMsg'); ?>
        </div>
        <?php
        }
        if($this->session->flashdata('errorMsg')){
            ?>
        <div class="alert alert-danger alert-dismissible" id="alert-error"">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <h4><i class="icon fa fa-warning"></i>Error!</h4>
            <span id="message-alert-error">Some error occured, please try again later</span>
        </div>
        <?php
    }
    ?>
    
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="stepwizard">
                    <div class="stepwizard-row">
                        <div class="stepwizard-step">
                            <a type="button" class="btn btn-primary btn-circle">1</a>
                            <!-- <p>Cart</p> -->
                        </div>
                        <div class="stepwizard-step">
                            <a type="button" class="btn btn-default btn-circle">2</a>
                            <!-- <p>Shipping</p> -->
                        </div>
                        <!-- <div class="stepwizard-step">
                            <a type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                            <p>Payment</p>
                        </div>  -->
                    </div>
                </div>
            </div>
            <div class="col-lg-3"></div>
        </div>
        <br>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-4">     
                                    <div class="form-group">
                                        <label>Pickup Address</label>
                                        <input class="form-control" id="pickupInput" type="text"/>                                         
                                    </div>
                                </div>        
                                <div class="col-lg-4">        
                                    <div class="form-group">
                                        <label>Drop Point Address</label>
                                        <input class="form-control" id="dropInput" type="text"/>                                          
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Pickup Time</label>
                                            <select class="form-control">
                                                <option>Select</option>
                                                <option disabled>9:00 </option>
                                                <option>9:15</option>
                                                <option>9:30</option>
                                            </select>                                         
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Drop Time</label>
                                            <select class="form-control">
                                                <option>Select</option>
                                                <option disabled>9:00 </option>
                                                <option>9:15</option>
                                                <option>9:30</option>
                                            </select>                                         
                                    </div>
                                </div>
                            </div>
                            <br>
                            <!-- /.col-lg-6 (nested) -->
                            <div class="row">
                                <div class="col-lg-4">        
                                    <div class="form-group">
                                        <label>From Location</label>
                                            <select class="form-control">
                                                <option>Select</option>
                                                <option>Panaji</option>
                                                <option>KTC Bus Stand</option>
                                                <option>GMC Bambolim</option>
                                            </select>                                            
                                    </div>
                                </div>        
                                <div class="col-lg-4">        
                                    <div class="form-group">
                                        <label>To Location</label>
                                            <select class="form-control">
                                                <option>Select</option>
                                                <option>Panaji</option>
                                                <option>KTC Bus Stand</option>
                                                <option>GMC Bambolim</option>
                                            </select>                                            
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                    <button class="btn btn-success single-button">Add</button>
                                </div>
                                <div class="col-lg-2">
                                    <button class="btn btn-primary single-button" onClick="vehicle_summary()">Calculate KM</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                    <th>From </th>
                                                    <th>To</th>
                                                    <th>KM</th>
                                                    <th>Action</th>                   
                                                    </tr>
                                                </thead>                    
                                            <tbody>
                                                <tr>
                                                    <td>Panaji KTC</td>
                                                    <td>GMC Bambolim</td>
                                                    <td>3 KM</td>
                                                    <td><a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>GMC Bambolim</td>
                                                    <td>Agassaim</td>
                                                    <td>5 KM</td>
                                                    <td><a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>Agassaim</td>
                                                    <td>Verna</td>
                                                    <td>10 KM</td>
                                                    <td><a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>
                                                </tr>
                                            </tbody>
                                            </table>
                                        </div>
                                </div>
                            </div>
                            <div class="vehicle-summary">
                                <label>Vehicle Travel Summary</label>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table">
                                                    <thead>
                                                        <tr>
                                                        <th>#</th>
                                                        <th>From </th>
                                                        <th>To</th>
                                                        <th>KM</th>
                                                        </tr>
                                                    </thead>                    
                                                <tbody>
                                                    <tr>
                                                    <td>1</td>
                                                        <td>Panaji</td>
                                                        <td>Donapaula</td>
                                                        <td>3 KM</td>
                                                    </tr>
                                                    <tr>
                                                    <td>2</td>
                                                        <td>Donapaula</td>
                                                        <td>Agassaim</td>
                                                        <td>5 KM</td>
                                                    </tr>
                                                    <tr>
                                                    <td>3</td>
                                                        <td>Agassaim</td>
                                                        <td>Verna</td>
                                                        <td>10 KM</td>
                                                    </tr>
                                                    <tr>
                                                    <td>4</td>
                                                        <td>Verna</td>
                                                        <td>Vasco Airport</td>
                                                        <td>10 KM</td>
                                                    </tr>
                                                    <tr>
                                                    <td>5</td>
                                                        <td>Vasco Airport</td>
                                                        <td>Panaji</td>
                                                        <td>10 KM</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label>Actual Traveling KM: </label>  
                                            <span> 85KM </span>                                                                                
                                        </div>
                                        <div class="form-group">
                                            <label>Vehicle Destination to Destination: </label>  
                                            <span> 40KM </span>                                                                                
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                                <label>Grand Total KM(Estimation): </label>  
                                                <span> 125KM </span>                                                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                        <a href="add_taxi_booking3.php" class="btn btn-success single-button">Confirm Booking</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <!-- <label style="margin-bottom:20px;text-align:center;"> BOOKING DETAILS </label> -->
                                <div class="form-inline">
                                    <div class="form-group">
                                        <div> 
                                            <label for="exampleInputEmail2">Vendor Admin:</label>
                                            <span > Laxmi Travels</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <div> 
                                            <label for="exampleInputEmail2">Pickup From:</label>
                                            <span > Panaji, </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div> 
                                            <label for="exampleInputEmail2"> Drop To:</label>
                                            <span > Vasco </span>
                                        </div>
                                     </div>
                                </div>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <div> 
                                            <label for="exampleInputEmail2">Type of Vehicle:</label>
                                            <span > AC Maruti WagonR</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <div> 
                                            <label for="exampleInputEmail2">Start Date:</label>
                                            <span > 10/05/2018, </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div> 
                                            <label for="exampleInputEmail2">End Date:</label>
                                            <span > 16/05/2018 </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-inline">
                                    <div class="form-group">
                                        <div> 
                                            <label for="exampleInputEmail2">No of Days:</label>
                                            <span > 03 Days</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <div> 
                                            <label for="exampleInputEmail2">Start Time:</label>
                                            <span > 11:30 AM, </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div> 
                                            <label for="exampleInputEmail2">End Time:</label>
                                            <span > 06:30 PM </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <div> 
                                            <label for="exampleInputEmail2">Taxi Hiring Segment:</label>
                                            <span > Full Day</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <div> 
                                            <label for="exampleInputEmail2">Seating Capacity:</label>
                                            <span > 04</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- /.row (nested) -->
                        </div>
                    <!-- /.panel-body -->
                   
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
    
    </section><!--//end content section -->
</div>
