<div class="content-wrapper">
    <section class="content-header">
        <h1>Add Vendor Admin<small></small></h1>
    </section>
        <section class="content">
        <div class="alert alert-dismissible" id="div-vendoradmin-new-alert" style="display: none;"></div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form role="form" method="POST" id="frm-vendoradmin-addnew" name="frm-vendoradmin-addnew">
                        
                            <div class="row">
                                <div class="col-lg-6">
                        
                                        <div class="form-group">
                                            <label>Registered Business Name</label>
                                            <?php if(isset($vendorId)){
                                                ?>
                                                <input class="form-control" type="text" name="vendor_admin_fname" value="<?php print $vendorDetails->name; ?>" readonly required>
                                                <input type="hidden" name="vendorId" id="vendorId" value="<?php print $vendorId; ?>">
                                                <?php
                                            }
                                            else{
                                                ?>
                                                <select class="form-control" id="vendorId" name="vendorId" required>
                                                    <option value="" disabled selected >Select Vendor</option>
                                                    <?php foreach($allVendors AS $eachVendor): ?>
                                                        <option value="<?php print $eachVendor->id; ?>"><?php print $eachVendor->name; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <?php

                                            } ?>

                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Staff Name*</label>
                                            <input class="form-control" type="text" id="staffname" name="staffname" autofocus required>
                                        </div>
                                        <div class="form-group">
                                            <label>Date Of Birth </label>
                                            <input data-provide="datepicker" data-date-format='dd-mm-yyyy' class="form-control" type="text" name="vendor_admin_dob" id="vendor_admin_dob" autocomplete="off" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Permanent Address*</label>
                                         
                                               <input type="text" class="form-control" rows="3" id="vendor_admin_peradd" name="vendor_admin_peradd" autofocus required>
                                        </div>

                                        <div class="form-group">
                                            <label>Temporary Address</label>
                                            <textarea class="form-control" rows="3" id="vendor_admin_tempadd" name="vendor_admin_tempadd" required></textarea>
                                            <script type="text/javascript" >
                                            function disableMyText(){
                                                if(document.getElementById("checkMe").checked == true){
                                                    document.getElementById('vendor_admin_tempadd').value = '';
                                                    document.getElementById("vendor_admin_tempadd").disabled = true;
                                                }else{
                                                    document.getElementById("vendor_admin_tempadd").disabled = false;
                                                }
                                            }
                                            </script>
                                            <p class="help-block"><input type="checkbox" id="checkMe" name="same" onclick="disableMyText()" > Same as Permanent </p>
                                        </div>
                                        <div class="form-group">
                                            <label>Email Id*</label>
                                            <input class="form-control" type="text" id="vendor_admin_email" name="vendor_admin_email" required>
                                        </div>
                                        
                                        
                                   </div>
                                    <!-- /.col-lg-6 (nested) -->
                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            <label>Primary Contact Number*</label>
                                            <input class="form-control" type="text" id="vendor_admin_pricontact" name="vendor_admin_pricontact" pattern=".{10}" oninvalid="setCustomValidity('Please enter a valid number (10 Numbers only)')" onchange="try{setCustomValidity('')}catch(e){}" autofocus required>
                                        </div>
                                        <div class="form-group">
                                            <label>Secondary Contact Number</label>
                                            <input class="form-control" type="text" id="vendor_admin_seccontact" name="vendor_admin_seccontact" pattern=".{10}" oninvalid="setCustomValidity('Please enter a valid number (10 Numbers only)')" onchange="try{setCustomValidity('')}catch(e){}">
                                        </div>

                                        <div class="form-group">
                                            <label>Date Of Joining (DD-MM-YYYY)*</label>
                                            <input data-provide="datepicker" data-date-format="dd-mm-yyyy" class="form-control" type="text" id="vendor_admin_joining" name="vendor_admin_joining" value="<?php echo date("d-m-Y");?>" required autocomplete="off">
                                        </div>
                                        
                                        <div class="form-group">
                                            <script type="text/javascript" >
                                            function enableMyText(){
                                                if(document.getElementById("vendor_admin_desig").value == "other"){
                                                    document.getElementById("other_design").disabled = false;
                                                }else{
                                                    document.getElementById("other_design").disabled = true;
                                                    document.getElementById("other_design").value = "";
                                                }
                                            }
                                            </script>
                                            <label>Designation*</label>
                                            <select class="form-control" id="vendor_admin_desig" name="vendor_admin_desig" onclick="enableMyText()" required>
                                                <option value="" disabled selected >Select Designation</option>
                                                <?php foreach($designations AS $eachDesig): ?>
                                                    <option value="<?php print $eachDesig->id; ?>"><?php print $eachDesig->title; ?></option>
                                                <?php endforeach; ?>
                                                <option  value="other" >Others</option>
                                            </select>
                                            <br/>
                                            <label>Specify (If others)</label>
                                            <textarea class="form-control" rows="1" id="other_design" name="other_design" required disabled></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Voter Id or Aadhar No.*</label>
                                            <input class="form-control" type="text" id="vendor_admin_voter" name="vendor_admin_voter" required>
                                        </div>                            
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-12">
                                        
                                                       <div class="form-group">
                                    <label>Services provided</label>
                                    <div class="row">
                                           <?php foreach($services AS $eachService): ?>
                                                <div class="col-lg-3">
                                                              
                                                            <div class="checkbox">
                                                                <label>
                                                                   
                                                                    <input type="checkbox" value="<?php print $eachService->id; ?>" name="services[]" id="<?php print $eachService->id; ?>" readonly ><?php print $eachService->name; ?>
                                                                  
                                                                </label>
                                                            </div>
                                                        
                                                </div> 
                                            <?php endforeach; ?>              
                                                     
                                    </div>                                                                                                                                          
                                </div>
                                        <!--    
                                            <div class="form-group">
                                                <label>Services provided</label>
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" value="1" name="taxi">Taxi
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" value="1" name="self_drive">Self Drive
                                                                    </label>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-3">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" value="1" name="activities">Activities
                                                                    </label>
                                                                </div>
                                                            </div>

                                                        </div>
                                            </div>
                                        
                                <div class="form-group">
                                    <label>Activity</label>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1" name="waterfall">Waterfall
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1" name="boat_cruising">Boat Cruising
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1" name="dolphin_safari">Dolphin Safari
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1" name="tours">Tours
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1" name="excursion">Excursion
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1" name="disco" >Disco
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group adventure">
                                    <label>Adventure type</label>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1" name="air">Air
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1" name="land">Land
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1" name="water">Water
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                -->
                                    </div>
                                </div>

                             
                            </div>
                        </div>
                       <div class="col-lg-12">
                                
                               <input type="submit" name="submit" value="Save" class="btn btn-success" >
                               <button type="reset" class="btn btn-info">Reset</button> 
                                <input name="" type="button" value="Cancel" class="btn btn-success" onClick="location.replace('vendor_admin_list.php');" class="btn btn-default">   
                                <!-- <a class="btn btn-info">Reset</a>  -->                 
                        </div>
                        
                        <!-- /.panel-body -->
                                    
                        </form>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </section>
</div>