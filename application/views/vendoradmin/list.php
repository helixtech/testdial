<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Vendor Admin
            <small></small>
        </h1>
    </section>
    
    <!-- Main content -->
    <section class="content">

    <div class="alert alert-dismissible" id="div-vendoradmin-list-alert" style="display: none;"></div>
     <?php
         if($this->session->flashdata('successMsg')){
            ?>
         <div class="alert alert-success alert-dismissible" id="alert-success"">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <h4><i class="icon fa fa-check"></i>Success!</h4> <?php echo $this->session->flashdata('successMsg'); ?>
        </div>
        <?php
        }
        if($this->session->flashdata('errorMsg')){
            ?>
        <div class="alert alert-danger alert-dismissible" id="alert-error"">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <h4><i class="icon fa fa-warning"></i>Error!</h4>
            <span id="message-alert-error">Some error occured, please try again later</span>
        </div>
        <?php
    }
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>Action</label>
                                <select class="form-control" name="vendoradmin_status" id="sel-vendoradmin-action">
                                    <option value=" " disabled="" selected="">Select</option>
                                    <option value="Confirm">Confirm</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Delete">Delete</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <input name="activate" value="Apply" class="btn btn-success"  id="btn-vendoradmin-action" style="margin-top:25px;" type="button">
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-success" href="<?php print site_url('VendorAdmin/addnew'); ?>"><i class="fa fa-plus"></i> Add New Admin</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover datatable" id="" width="100%">
                        <thead>
                            <tr>
                                <th style="width: 100px;"><input name="chk[]"  id="chk-vendoradmin-list-head" type="checkbox">&nbsp;&nbsp;Mark All</th>
                                <th>Id </th>
                                <th>Name of admin</th>
                                <th>Email Id</th>
                                <th>Contact Number</th>
                                <th>Address</th>
                                <th>Joining Date</th>
                                <th>Status</th>
                               
                                <th style="width: 65px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($adminvendors AS $k=>$vendor): ?>
                                <tr>
                                    <td>
                                        <div class="checkbox">
                                            <label><input name="chk[]" class="vendoradmin-list-chkbxes" value="<?php print $vendor->user_id; ?>" type="checkbox"></label>
                                        </div>
                                    </td>
                                    <td><?php print $vendor->id; ?></td>
                                    <td><?php print $vendor->name; ?></td>
                                    <td><?php print $vendor->email; ?></td>
                                    <td><?php print $vendor->phone1; ?></td>                            
                                    <td><?php print $vendor->permanent_address; ?></td>                            
                                    <td><?php print $vendor->joiningdate; ?></td>                            
                                    <td>
                                        <?php if($vendor->status==0): ?>
                                            <small class="label pull-right bg-yellow">Pending</small>
                                        <?php elseif($vendor->status==1): ?>
                                            <small class="label pull-right bg-green">Confirmed</small>
                                        <?php elseif($vendor->status==-1): ?>
                                            <small class="label pull-right bg-red">Rejected</small>
                                        <?php endif; ?>
                                    </td>
                                    <th><a href="<?php print site_url('VendorAdmin/edit/'.$vendor->id); ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a> &nbsp;&nbsp;</th>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!--<div id="modal-fixedpointfare-delete" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4>Are you Sure want to Delete?</h4>
                </div>
                <div class="modal-footer">
                    <form method="post" id="frm-fixedpointfare-delete" action="<?php //print site_url('FixedPointFare/delete'); ?>">
                        <input type="hidden" name="fixedpointfareId" value="" id="inpt-fixedpointfare-delete-id"/>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>                                     
                    </form>
                </div>
            </div>
        </div>
    </div>-->
    
    </section>
</div>
